import Component from "./Component.js";

export default class Img extends Component {
	constructor(attribute) {
		super('img', {name:"src", value:attribute}, null);
	}
}