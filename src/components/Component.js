export default class Component {
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		let res;
		if(this.children == null) {
			res = `<${this.tagName} `;
			if(this.attribute != null) res += this.renderAttribute();
			res += `/>`;
			return res;
		} else if (this.children)
		return `<${this.tagName}>${this.children}</${this.tagName}>`;
	}

	renderAttribute() {
		return `${this.attribute.name}="${this.attribute.value}"`;
    }
    
    renderChildren() {
        return null; //TODO
    }
}